patient_data = {}
welcome_promt = "Welcome doctor! What would you like to do today?\n 1. New Diagnosis\n 2. List all patients\n q. Quit\n"
patient_name_prompt = "What is the patient's name?\n"
general_appearance_prompt = "What is the patient's general appearance?\n 1. Normal apprearance\n 2. Irritable or lethargic\n"
skin_prompt = "What is the patient's skin response to pinching?\n 1. Normal skin\n 2. Sking pinch recovery is slow\n"
eyes_prompt = "How does the patient's eyes look?\n 1. Normal or slightly sunken eyes\n 2. Very sunken eyes\n"

def check_general_appearance():
    # Check the general appearance of the patient
    appearance = input(general_appearance_prompt)
    if appearance == "1":
        return check_eyes()
    elif appearance == "2":
        return check_skin()
    else:
        print("Invalid input")
        check_general_appearance()

def check_skin():
    # Check the skin of the patient
    skin = input(skin_prompt)
    if skin == "1":
        return "Some Dehydration"
    elif skin == "2":
        return "Severe Dehydration"
    else:
        print("Invalid input")
        check_skin()

def check_eyes():
    # Check the eyes of the patient
    eyes = input(eyes_prompt)
    if eyes == "1":
        return "No Dehydration"
    elif eyes == "2":
        return "Severe Dehydration"
    else:
        print("Invalid input")
        check_eyes()

def newdiag():
    # Run a new diagnosis
    patient_name = input(patient_name_prompt)
    diagnosis = check_general_appearance()
    if patient_name=="" or diagnosis=="":
        print("Invalid input")
        newdiag()
    print("The diagnosis for " + patient_name + " is " + diagnosis)
    patient_data[patient_name] = diagnosis
    return False

def returnolddiag():
    # Return the old diagnosis for a patient
    print("Here are all the patients and their diagnoses:")
    for patient in patient_data:
        print(patient + ": " + patient_data[patient])
    return False


def main():
    # Main function
    while True:
        user_selection = input(welcome_promt)
        if user_selection == "1":
            newdiag()
        elif user_selection == "2":
            returnolddiag()
        elif user_selection == "q":
            print("Goodbye!")
            return True

main()